import java.time.LocalDate;
import java.util.Date;


public class Personne  {
    private String prenom;
    private String nom;
//    private LocalDate dateDeNaissance;

    public Personne() {
    }

    /*
    public Personne(String prenom, String nom, LocalDate dateDeNaissance) {
        this.prenom = prenom;
        this.nom = nom;
        this.dateDeNaissance = dateDeNaissance;
    }
*/
    public Personne(String prenom, String nom) {
        this.prenom = prenom;
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }



    /*
        public LocalDate getDateDeNaissance() {
            return dateDeNaissance;
        }

        public void setDateDeNaissance(LocalDate dateDeNaissance) {
            this.dateDeNaissance = dateDeNaissance;
        }
    */
    @Override
    public String toString() {
        return "Personne{" +
                "prenom='" + prenom + '\'' +
                ", nom='" + nom + '\'' +
               // ", dateDeNaissance=" + dateDeNaissance +
                '}';
    }
}
