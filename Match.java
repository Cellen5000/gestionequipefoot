import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class Match extends Evenement implements IAfficherStatistiques{
    TypeEvenement typeMatch = TypeEvenement.MATCH;
    SelectionPourMatch s1;
    int[] resultatMatch = new int[2];

    String nomEquipe;
    String nomEquipeAdverse;





    int nbrePassesDecisivesTotales = 0;

    int nbreBallesMarqueesTotales = 0;

    int nbreTentativesTirsManquesTotales = 0;

    int nbreCartonsJaunesTotal=0;
    int nbreCartonsRougesTotal=0;

    int nbreFautesCommisesTotales = 0;
    int nbreFautesSubiesTotal=0;

    int nbreBallesRetenuesTotal = 0;
    int nbreButEncaissesTotal = 0;



    public Match(String lieu, SelectionPourMatch s1, String nomEquipeAdverse) {
        super(lieu);
        this.s1 = s1;
        nomEquipe = s1.getEquipeMatch().getNomEquipe();
        this.nomEquipeAdverse = nomEquipeAdverse;
        for ( JoueurAutreQueGardien j : s1.listeJoueursEquipeSelectionnables){
            j.initCompteursPersoPourMatch();
            j.jouer();
            j.nbreMatchJoues ++;
            resultatMatch[0] += j.nbreBallesMarqueesEn1Match;

        }
        s1.getEquipeMatch().getGardien().initCompteursPersoPourMatch();
        s1.getEquipeMatch().getGardien().jouer();
        resultatMatch[1] = s1.getEquipeMatch().getGardien().getNbreButEncaissesEn1Match();
        s1.getEquipeMatch().getGardien().setNbreMatchJoues(s1.getEquipeMatch().getGardien().getNbreMatchJoues()+1);
        s1.getEquipeMatch().getEntraineur().setNbreMatchesCoaches(s1.getEquipeMatch().getEntraineur().getNbreMatchesCoaches()+1);
        if (resultatMatch[0] > resultatMatch[1] ){
            s1.getEquipeMatch().getEntraineur().setNbreMatchesRemportes(s1.getEquipeMatch().getEntraineur().getNbreMatchesRemportes()+1);
        }


    }

    public TypeEvenement getTypeMatch() {
        return typeMatch;
    }

    public void setTypeMatch(TypeEvenement typeMatch) {
        this.typeMatch = typeMatch;
    }

    public SelectionPourMatch getS1() {
        return s1;
    }

    public void setS1(SelectionPourMatch s1) {
        this.s1 = s1;
    }

    public int[] getResultatMatch() {
        return resultatMatch;
    }

    public void setResultatMatch(int[] resultatMatch) {
        this.resultatMatch = resultatMatch;
    }

    @Override
    public String toString() {
        return "Match{" +
                "Numero evenement='" + numero + '\'' +
                ", typeMatch=" + typeMatch +
                ", lieu='" + lieu + '\'' +
                ", resultatMatch=" + Arrays.toString(resultatMatch) +
                ", nomEquipe='" + nomEquipe + '\'' +
                ", nomEquipeAdverse='" + nomEquipeAdverse + '\'' +
                ", s1=" + s1 + '\'' +
                '}';
    }

    /**
     * Méthode redéfinie qui permet d'afficher les statistiques du match
     */
    @Override
    public void afficherStat() {
        this.calculPourStat();
        System.out.printf("MATCH %s contre %s à %s\n", s1.equipeMatch.getNomEquipe(), nomEquipeAdverse, lieu );
        System.out.printf("Résultat: %d - %d\n", resultatMatch[0], resultatMatch[1]);

        System.out.printf("Dans ce match, pour %s : \n", s1.equipeMatch.getNomEquipe());
        System.out.printf("Le nombre de fautes commises au total du Match est de %d:\n", nbreFautesCommisesTotales);
        System.out.printf("dont %d cartons jaunes et %d cartons rouges.\n", nbreCartonsJaunesTotal,nbreCartonsRougesTotal);
        for (Joueur j : this.s1.listeJoueursEquipeSelectionnables) {
            if (j.getNbreFautesCommisesEn1Match() >0 )
                System.out.println(j.getPrenom()+j.getNom()+" a "+ j.getNbreCartonsJaunesEn1Match()+" cartons jaunes et "+ j.getNbreCartonsRougesEn1Match() + " carton rouge.");
        }
        System.out.println(s1.getEquipeMatch().getGardien().getPrenom()+s1.getEquipeMatch().getGardien().getNom()+" a "+ s1.getEquipeMatch().getGardien().getNbreCartonsJaunesEn1Match()+" cartons jaunes et "+ s1.getEquipeMatch().getGardien().getNbreCartonsRougesEn1Match() + " carton rouge.");
        System.out.printf("Sur %d ballons:\n", nbreBallesMarqueesTotales + nbreTentativesTirsManquesTotales);
        System.out.printf("Le nombre de balles marquées au total du Match est de %d:\n", nbreBallesMarqueesTotales);
        for (JoueurAutreQueGardien j : this.s1.listeJoueursEquipeSelectionnables) {
            if (j.getNbreBallesMarqueesEn1Match() >0 )
                System.out.println(j.getPrenom()+j.getNom()+" a marqué "+ j.getNbreBallesMarqueesEn1Match()+" goals.");
        }
        System.out.printf("Le nombre de tentatives de tirs manquées au total du Match est de %d:\n", nbreTentativesTirsManquesTotales);
        for (JoueurAutreQueGardien j : this.s1.listeJoueursEquipeSelectionnables) {
            if (j.getNbreTentativesTirsManquesEn1Match() >0 )
                System.out.println(j.getPrenom()+j.getNom()+" a raté "+ j.getNbreBallesMarqueesEn1Match()+" tentatives de tirs.");
        }
        System.out.printf("Le nombre de tentatives de passes décisives au total du Match est de %d:\n", nbrePassesDecisivesTotales);
        for (JoueurAutreQueGardien j : this.s1.listeJoueursEquipeSelectionnables) {
            if (j.getNbrePassesDecisivesEn1Match() >0 )
                System.out.println(j.getPrenom()+j.getNom()+" a fait "+ j.getNbrePassesDecisivesEn1Match()+" passes décisives.");
        }
        System.out.println(s1.getEquipeMatch().getGardien().getPrenom()+s1.getEquipeMatch().getGardien().getNom()+" a "+ s1.getEquipeMatch().getGardien().getNbreCartonsJaunesEn1Match()+" cartons jaunes et "+ s1.getEquipeMatch().getGardien().getNbreCartonsRougesEn1Match() + " carton rouge.");
        System.out.printf("Sur %d ballons:\n", s1.getEquipeMatch().getGardien().nbreBallesRetenuesEn1Match+ s1.getEquipeMatch().getGardien().nbreButEncaissesEn1Match);
        System.out.println(s1.getEquipeMatch().getGardien().getPrenom()+s1.getEquipeMatch().getGardien().getNom()+" a rattrapé "+ s1.getEquipeMatch().getGardien().nbreBallesRetenuesEn1Match+" ballons et a encaissé "+ s1.getEquipeMatch().getGardien().nbreButEncaissesEn1Match + " buts.");
    }

    /**
     * Méthode redéfinie qui permet de faire des calculs pour les statistiques du match
     */
    @Override
    public void calculPourStat() {
        for (Joueur j : this.s1.listeJoueursEquipeSelectionnables) {
            nbreCartonsJaunesTotal += j.getNbreCartonsJaunesEn1Match();
            nbreCartonsRougesTotal += j.getNbreCartonsRougesEn1Match();
            nbreFautesCommisesTotales = nbreCartonsJaunesTotal + nbreCartonsRougesTotal;
        }
        for (JoueurAutreQueGardien j : this.s1.listeJoueursEquipeSelectionnables){
            nbreBallesMarqueesTotales += j.nbreBallesMarqueesEn1Match;
            nbreTentativesTirsManquesTotales += j.nbreTentativesTirsManquesEn1Match;
            nbrePassesDecisivesTotales += j.nbrePassesDecisivesEn1Match;
        }
    }
}
