import java.util.HashSet;

public class Equipe {
    private String nomEquipe;

    private Entraineur entraineur;
    private Gardien gardien;
    HashSet<JoueurAutreQueGardien> listeJoueurs = new HashSet<>();

    public Equipe(String nomEquipe, Entraineur entraineur,Gardien gardien,JoueurAutreQueGardien... autresJoueurs ) {
        this.nomEquipe = nomEquipe;
        this.entraineur = entraineur;
        this.gardien = gardien;
        for (JoueurAutreQueGardien temp : autresJoueurs) {
            listeJoueurs.add(temp);
        }

        if (autresJoueurs.length < 17){
            System.out.println("Rajoutez encore d'autres joueurs dans cette équipe");
        }

    }

    public String getNomEquipe() {
        return nomEquipe;
    }

    public void setNomEquipe(String nomEquipe) {
        this.nomEquipe = nomEquipe;
    }

    public Entraineur getEntraineur() {
        return entraineur;
    }

    public void setEntraineur(Entraineur entraineur) {
        this.entraineur = entraineur;
    }

    public Gardien getGardien() {
        return gardien;
    }

    public void setGardien(Gardien gardien) {
        this.gardien = gardien;
    }

    public HashSet<JoueurAutreQueGardien> getListeJoueurs() {
        return listeJoueurs;
    }

    public void setListeJoueurs(HashSet<JoueurAutreQueGardien> listeJoueurs) {
        this.listeJoueurs = listeJoueurs;
    }

    @Override
    public String toString() {
        return "Equipe{" +
                "nomEquipe='" + nomEquipe + '\'' +
                ", entraineur=" + entraineur +
                ", gardien=" + gardien +
                ", listeJoueurs=" + listeJoueurs +
                '}';
    }
}
