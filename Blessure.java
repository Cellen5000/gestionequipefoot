import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class Blessure {
    private boolean intentionnelle;
    TypeBlessure typeBlessure;
    Joueur emmetteurBlessure;
    Joueur recepteurBlessure;

    Evenement evenement;


    /*
    HashSet<Joueur> listeJoueurs = new HashSet<>();
    HashSet<Evenement> listeEvenement = new HashSet<>();
    HashMap<Evenement, HashSet<Joueur>> listeJoueurParEvenement = new HashMap<>();
    HashMap<Joueur, HashSet<Evenement>> listeEvenementParJoueur = new HashMap<>();

    public Blessure(boolean intentionnelle, TypeBlessure typeBlessure, Joueur emmetteurBlessure, Joueur recepteurBlessure, Evenement evenement) {
        this.intentionnelle = intentionnelle;
        this.typeBlessure = typeBlessure;
        this.emmetteurBlessure = emmetteurBlessure;
        this.recepteurBlessure = recepteurBlessure;
        this.evenement = evenement;
        this.recepteurBlessure.setBlesse(true);
        this.recepteurBlessure.setStamina(getRecepteurBlessure().getStamina()-1);
        this.recepteurBlessure.setNbreBlessuresTotal(getRecepteurBlessure().nbreBlessuresTotal+1);
    }
*/
    public Blessure(TypeBlessure typeBlessure, Joueur recepteurBlessure, Evenement evenement) throws ExceptionParticuliere {
        this.typeBlessure = typeBlessure;
        this.recepteurBlessure = recepteurBlessure;
        this.evenement = evenement;
        this.recepteurBlessure.setBlesse(true);

    }

    public boolean isIntentionnelle() {
        return intentionnelle;
    }

    public void setIntentionnelle(boolean intentionnelle) {
        this.intentionnelle = intentionnelle;
    }

    public TypeBlessure getTypeBlessure() {
        return typeBlessure;
    }

    public void setTypeBlessure(TypeBlessure typeBlessure) {
        this.typeBlessure = typeBlessure;
    }

    public Joueur getEmmetteurBlessure() {
        return emmetteurBlessure;
    }

    public void setEmmetteurBlessure(Joueur emmetteurBlessure) {
        this.emmetteurBlessure = emmetteurBlessure;
    }

    public Joueur getRecepteurBlessure() {
        return recepteurBlessure;
    }

    public void setRecepteurBlessure(Joueur recepteurBlessure) {
        this.recepteurBlessure = recepteurBlessure;
    }

    @Override
    public String toString() {
        return "Blessure{" +
                "intentionnelle=" + intentionnelle +
                ", typeBlessure=" + typeBlessure +
                ", EmmetteurBlessure=" + emmetteurBlessure +
                ", RecepteurBlessure=" + recepteurBlessure +
                '}';
    }
}
