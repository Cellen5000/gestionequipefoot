public class GestionJoueursFoot {
    public static void main(String[] args) throws ExceptionParticuliere {
        // String prenom, String nom, int nbreCartonsJaunesTotal, int nbreCartonsRougesTotal, int nbreMatchsManques, int nbreMatchJoues, int nbreEntrainementsJoues, int nbreEntrainementsManques, int nbreFautesSubiesTotal, int nbreFautesCommisesTotales, int nbreBallesRetenuesTotal, int nbreButEncaissesTotal) {
        //        super(prenom, nom, nbreCartonsJaunesTotal, nbreCartonsRougesTotal, nbreMatchsManques, nbreMatchJoues, nbreEntrainementsJoues, nbreEntrainementsManques, nbreFautesSubiesTotal, nbreFautesCommisesTotales);
        //Personne geq1 = new Gardien("Thibaut", "Courtois", 21/5/92, 0,0,8,88,26,200,145,3 );
        //Personne geq1 = new Gardien("Thibaut", "Courtois", 1, 0,8,88,200,26,3,1 ,145,3);
        //Gardien g2eq1 = new Gardien("Simon", "Signolet");


        Entraineur entraineur = new Entraineur("Roberto", "Martinez");
        System.out.println(entraineur);

        System.out.println("______________________________________________________");
        Gardien g1eq1 = new Gardien("Thibaut", "Courtois");
        System.out.println(g1eq1);

        System.out.println("______________________________________________________");
        JoueurAutreQueGardien j1eq1 = new JoueurAutreQueGardien("Eden", "Hazard", Position.ATTAQUANT_CENTRE, Position.MILIEU_CENTRE_GAUCHE,Position.ATTAQUANT_DROIT, PiedFort.AMBIDEXTRE );
        JoueurAutreQueGardien j2eq1 = new JoueurAutreQueGardien("Thogan", "Hazard", Position.ATTAQUANT_GAUCHE, Position.MILIEU_CENTRE_GAUCHE,Position.MILIEU_CENTRE_DROIT, PiedFort.AMBIDEXTRE );
        JoueurAutreQueGardien j3eq1 = new JoueurAutreQueGardien("Romelu", "Lukaku", Position.ATTAQUANT_CENTRE, Position.ATTAQUANT_GAUCHE,Position.ATTAQUANT_DROIT, PiedFort.GAUCHE );
        JoueurAutreQueGardien j4eq1 = new JoueurAutreQueGardien("Kevin", "De Bruyne", Position.MILIEU_CENTRE_DROIT, Position.MILIEU_CENTRE_GAUCHE,Position.MILIEU_LATTERAL_DROIT, PiedFort.DROIT );
        JoueurAutreQueGardien j5eq1 = new JoueurAutreQueGardien("Jan", "Vertonghen", Position.DEFENSEUR_GAUCHE, Position.DEFENSEUR_CENTRE,Position.DEFENSEUR_DROIT, PiedFort.GAUCHE );
        JoueurAutreQueGardien j6eq1 = new JoueurAutreQueGardien("Thomas", "Meunier", Position.DEFENSEUR_DROIT, Position.MILIEU_LATTERAL_DROIT,Position.MILIEU_CENTRE_DROIT, PiedFort.DROIT );
        JoueurAutreQueGardien j7eq1 = new JoueurAutreQueGardien("Axel", "Witsel", Position.MILIEU_CENTRE_DROIT, Position.MILIEU_CENTRE_GAUCHE,Position.DEFENSEUR_CENTRE, PiedFort.DROIT );
        JoueurAutreQueGardien j8eq1 = new JoueurAutreQueGardien("Toby", "Alderweireld", Position.DEFENSEUR_GAUCHE, Position.DEFENSEUR_CENTRE,Position.DEFENSEUR_DROIT, PiedFort.DROIT );
        JoueurAutreQueGardien j9eq1 = new JoueurAutreQueGardien("Yannick", "Carrasco", Position.MILIEU_LATTERAL_GAUCHE, Position.MILIEU_CENTRE_GAUCHE,Position.ATTAQUANT_GAUCHE, PiedFort.AMBIDEXTRE );
        JoueurAutreQueGardien j10eq1 = new JoueurAutreQueGardien("Dries", "Mertens", Position.ATTAQUANT_CENTRE, Position.MILIEU_CENTRE_GAUCHE,Position.MILIEU_CENTRE_DROIT, PiedFort.DROIT );
        JoueurAutreQueGardien j11eq1 = new JoueurAutreQueGardien("Youri", "Tielemans", Position.MILIEU_CENTRE_DROIT, Position.MILIEU_CENTRE_GAUCHE,Position.MILIEU_LATTERAL_DROIT, PiedFort.AMBIDEXTRE );
        JoueurAutreQueGardien j12eq1 = new JoueurAutreQueGardien("Alexis", "Saelemaekers", Position.MILIEU_LATTERAL_DROIT, Position.MILIEU_LATTERAL_GAUCHE,Position.ATTAQUANT_DROIT, PiedFort.DROIT );
        JoueurAutreQueGardien j13eq1 = new JoueurAutreQueGardien("Dedryck", "Boyata", Position.DEFENSEUR_GAUCHE, Position.DEFENSEUR_CENTRE,Position.DEFENSEUR_DROIT, PiedFort.DROIT );
        JoueurAutreQueGardien j14eq1 = new JoueurAutreQueGardien("Jason", "Denayer", Position.DEFENSEUR_GAUCHE, Position.DEFENSEUR_CENTRE,Position.DEFENSEUR_DROIT, PiedFort.DROIT );
        JoueurAutreQueGardien j15eq1 = new JoueurAutreQueGardien("Charles", "De Ketelaere", Position.MILIEU_LATTERAL_GAUCHE, Position.MILIEU_CENTRE_GAUCHE,Position.ATTAQUANT_GAUCHE, PiedFort.GAUCHE );


        //
        Equipe eq1 = new Equipe("Les Diables Rouges", entraineur, g1eq1, j1eq1, j2eq1, j3eq1,j4eq1,j5eq1,j6eq1,j7eq1,j8eq1,j9eq1,j10eq1,j11eq1,j12eq1,j13eq1, j14eq1, j15eq1);
        System.out.println(eq1.listeJoueurs);

        //
        System.out.println("______________________________________________________");
        SelectionPourEntrainement selectioneq1Entrainement1 = new SelectionPourEntrainement(eq1);
        System.out.println(selectioneq1Entrainement1.listeJoueursEquipeSelectionnables);
        System.out.println(selectioneq1Entrainement1);

        System.out.println("______________________________________________________");
        Evenement e1 = new Entrainement("Bruxelles", selectioneq1Entrainement1);
        System.out.println(e1);

        System.out.println("______________________________________________________");
        Presence presence1 = new Presence(e1,j1eq1);
        System.out.println(presence1);

        System.out.println("______________________________________________________");
        SelectionPourMatch selectioneq1Match1 = new SelectionPourMatch(eq1);
        System.out.println(selectioneq1Match1.listeJoueursEquipeSelectionnables);
        System.out.println(selectioneq1Match1.listeDePositions);
        System.out.println(selectioneq1Match1);

        System.out.println("______________________________________________________");
        System.out.println(j1eq1);
        j1eq1.sEntrainer();
        System.out.println(j1eq1);
        j1eq1.seBlesser();
        System.out.println(j1eq1);
        j1eq1.guerir();
        System.out.println(j1eq1);

        System.out.println("______________________________________________________");
        System.out.println(g1eq1);
        g1eq1.jouer();
        System.out.println(g1eq1);

        System.out.println("______________________________________________________");
        Match match1 = new Match("Stade de Bruxelles",selectioneq1Match1, "PSG");
        System.out.println(match1);


        System.out.println("______________________________________________________");
        Faute f1 = new Faute(Cartons.PREMIER_CARTON_JAUNE, j1eq1, match1);
        System.out.println(f1);
        System.out.println(j1eq1);

        Faute f2 = new Faute(Cartons.DEUXIEME_CARTON_JAUNE, j1eq1, match1);
        System.out.println(f2);
        System.out.println(j1eq1);

        Faute f3 = new Faute(Cartons.CARTON_ROUGE, j1eq1, match1);
        System.out.println(f3);


        System.out.println("______________________________________________________");

        j1eq1.afficherStat();

        System.out.println("\n______________________________________________________\n");
        g1eq1.sEntrainer();
        Faute f4 = new Faute(Cartons.DEUXIEME_CARTON_JAUNE, g1eq1, match1);
        g1eq1.afficherStat();

        System.out.println("\n______________________________________________________\n");
        match1.afficherStat();

        System.out.println("\n______________________________________________________\n");
        Evenement e2 = new Entrainement("Bruxelles", selectioneq1Entrainement1);
        System.out.println(j1eq1.isBlesse);
        try{
            Blessure blessure1 = new Blessure(TypeBlessure.ENTORSE, j1eq1, e2);
            System.out.println(blessure1);
            j1eq1.ajouterBlessureAListeBlessures(blessure1);
            System.out.println(j1eq1.getListeBlessures());
            System.out.println(j1eq1.isBlesse);
        } catch (ExceptionParticuliere e){
            e.printStackTrace();
        }


    }
}
