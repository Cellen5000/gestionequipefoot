import java.util.HashMap;
import java.util.HashSet;

public class Faute {
    Cartons cartonRecu;
    Joueur joueur;
    Match match;

    HashSet<Joueur> listeJoueurs = new HashSet<>();
    HashSet<Match> listeMatch = new HashSet<>();
    HashSet<Cartons> listeCartonsRecus = new HashSet<>();
    HashMap<Match, HashSet<Cartons>> listeCartonsParMatch = new HashMap<>();
    HashMap<Joueur, HashSet<Cartons>> listeCartonsParJoueur = new HashMap<>();


    public Faute(Cartons cartonRecu, Joueur joueur, Match match) {
        this.cartonRecu = cartonRecu;
        this.joueur = joueur;
        this.match = match;
        if (cartonRecu ==Cartons.PREMIER_CARTON_JAUNE){
            this.joueur.nbreCartonsJaunesEn1Match = 1;
            this.joueur.nbreCartonsJaunesTotal += this.joueur.nbreCartonsJaunesEn1Match;
        }
        if (cartonRecu ==Cartons.DEUXIEME_CARTON_JAUNE){
            this.joueur.nbreCartonsJaunesEn1Match = 2;
            this.joueur.nbreCartonsJaunesTotal += this.joueur.nbreCartonsJaunesEn1Match;
        }
        if (cartonRecu ==Cartons.CARTON_ROUGE){
            this.joueur.nbreCartonsJaunesEn1Match = 2;
            this.joueur.nbreCartonsJaunesTotal += this.joueur.nbreCartonsJaunesEn1Match;
            this.joueur.nbreCartonsRougesEn1Match = 1;
            this.joueur.nbreCartonsRougesTotal += this.joueur.nbreCartonsRougesEn1Match;
        }
        listeCartonsRecus.add(cartonRecu);
        listeJoueurs.add(joueur);
        listeMatch.add(match);
    }



    public Cartons getCartonRecu() {
        return cartonRecu;
    }

    public void setCartonRecu(Cartons cartonRecu) {
        this.cartonRecu = cartonRecu;
    }

    public Joueur getJoueur() {
        return joueur;
    }

    public void setJoueur(Joueur joueur) {
        this.joueur = joueur;
    }

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    @Override
    public String toString() {
        return "Faute{" +
                "cartonRecu=" + cartonRecu +
                ", joueur=" + joueur +
                ", match=" + match +
                '}';
    }
}
