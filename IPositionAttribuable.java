import java.util.HashSet;

public interface IPositionAttribuable {
    /**
     * Méthode déclarée et à redéfinir qui permet d'attribuer un joueur pour chaque position pour aller en Match
     * @param listeJoueursEquipeSelectionnable liste sans doublons de joueurs d'une équipe sélectionnée
     * Pour être selectionné: le joueur doit avoir dans sa specialisation  (+ la stamina et la qlitéJeu les + hauts)
     */
    public void attribuerJoueurA1Position(HashSet<JoueurAutreQueGardien> listeJoueursEquipeSelectionnable);
}
