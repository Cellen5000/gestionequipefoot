public interface IAfficherStatistiques {
    /**
     * Méthode déclarée et à redéfinir qui permet d'afficher les statistiques
     */
    public void afficherStat();

    /**
     * Méthode déclarée et à redéfinir qui permet de faire des calculs pour les statistiques
     */
    public void calculPourStat();
}
