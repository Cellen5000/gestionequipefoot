import java.util.Date;

public class Entraineur extends Personne{
    Role role = Role.ENTRAINEUR;
    Equipe equipeActuelle;
    int nbreMatchesCoaches;
    int nbreMatchesRemportes;
    int nbreEntrainements;

    public Entraineur() {
        super();
    }

    public Entraineur(String prenom, String nom) {
        super(prenom, nom);

    }

    public Entraineur(String prenom, String nom,  Equipe equipeActuelle) {
        super(prenom, nom);
        this.equipeActuelle = equipeActuelle;
    }

   /*
    public Entraineur(String prenom, String nom,  Equipe equipeActuelle, int nbreMatchesCoaches, int nbreMatchesRemportés, int nbreEntrainements) {
        super(prenom, nom);
        this.equipeActuelle = equipeActuelle;
        this.nbreMatchesCoaches = nbreMatchesCoaches;
        this.nbreMatchesRemportés = nbreMatchesRemportés;
        this.nbreEntrainements = nbreEntrainements;
    }
*/
    public Role getRole() {
        return role;
    }


    public void setRole(Role role) {
        this.role = role;
    }

    public int getNbreMatchesCoaches() {
        return nbreMatchesCoaches;
    }

    public void setNbreMatchesCoaches(int nbreMatchesCoaches) {
        this.nbreMatchesCoaches = nbreMatchesCoaches;
    }

    public int getNbreMatchesRemportes() {
        return nbreMatchesRemportes;
    }

    public void setNbreMatchesRemportes(int nbreMatchesRemportes) {
        this.nbreMatchesRemportes = nbreMatchesRemportes;
    }

    public int getNbreEntrainements() {
        return nbreEntrainements;
    }

    public void setNbreEntrainements(int nbreEntrainements) {
        this.nbreEntrainements = nbreEntrainements;
    }

    @Override
    public String toString() {
        return "Entraineur{" +
                "role=" + getRole() +
                ", equipeActuelle=" + equipeActuelle +
                ", prenom='" + super.getPrenom() + '\'' +
                ", nom='" + super.getNom() + '\'' +
                //", dateDeNaissance=" + super.getDateDeNaissance() +
                ", nbreMatchesCoaches=" + nbreMatchesCoaches +
                ", nbreMatchesRemportés=" + nbreMatchesRemportes +
                ", nbreEntrainements=" + nbreEntrainements +
                '}';
    }
}
