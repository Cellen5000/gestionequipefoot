import java.util.HashSet;

public interface ISelectionnable {
    /**
    * Interface qui contient la déclaration des méthodes concernant la selection de joueurs d'une équipe de foot pour aller en Match
    */

    /**
     * Méthode déclarée et à redéfinir qui permet de savoir si un joueur d'une équipe est séléctionnable pour aller en Match
     * @param j liste un joueur à évaluer pour une sélection
     * @return true s'il répond aux conditions suivantes:
     * Pour être selectionné: le joueur doit être disponible, ne pas être blessé, avoir un stamina sup. ou égal à 7
     */
    public boolean isSelectionnable(JoueurAutreQueGardien j);

    /**
     * Méthode déclarée et à redéfinir qui permet de sélectionner joueurs d'une équipe de foot pour aller en Match
     * @param listeJoueursEquipeComplete liste sans doublons de joueurs d'une équipe
     * Pour être selectionné: le joueur doit être sélectionnable
     */
    public void selectionner(HashSet<JoueurAutreQueGardien> listeJoueursEquipeComplete);


}
