import java.util.HashMap;
import java.util.HashSet;

public class Presence {
    Joueur j;
    Evenement e;
    HashSet<Joueur> listeJoueurs = new HashSet<>();
    HashSet<Evenement> listeEvenement = new HashSet<>();
    HashMap<Evenement, HashSet<Joueur>> listeJoueurParEvenement = new HashMap<>();
    HashMap<Joueur, HashSet<Evenement>> listeEvenementParJoueur = new HashMap<>();

    public Presence(Evenement e, Joueur j) {
        listeEvenement.add(e);
        listeJoueurs.add(j);
        listeEvenementParJoueur.put(j, listeEvenement);
        listeJoueurParEvenement.put(e, listeJoueurs);
    }

    public Joueur getJ() {
        return j;
    }

    public void setJ(Joueur j) {
        this.j = j;
    }

    public Evenement getE() {
        return e;
    }

    public void setE(Evenement e) {
        this.e = e;
    }

    public HashSet<Joueur> getListeJoueurs() {
        return listeJoueurs;
    }

    public void setListeJoueurs(HashSet<Joueur> listeJoueurs) {
        this.listeJoueurs = listeJoueurs;
    }

    public HashSet<Evenement> getListeEvenement() {
        return listeEvenement;
    }

    public void setListeEvenement(HashSet<Evenement> listeEvenement) {
        this.listeEvenement = listeEvenement;
    }

    public HashMap<Evenement, HashSet<Joueur>> getListeJoueurParEvenement() {
        return listeJoueurParEvenement;
    }

    public void setListeJoueurParEvenement(HashMap<Evenement, HashSet<Joueur>> listeJoueurParEvenement) {
        this.listeJoueurParEvenement = listeJoueurParEvenement;
    }

    public HashMap<Joueur, HashSet<Evenement>> getListeEvenementParJoueur() {
        return listeEvenementParJoueur;
    }

    public void setListeEvenementParJoueur(HashMap<Joueur, HashSet<Evenement>> listeEvenementParJoueur) {
        this.listeEvenementParJoueur = listeEvenementParJoueur;
    }

    @Override
    public String toString() {
        return "Presence{" +
                "j=" + j +
                ", e=" + e +
                ", listeJoueurs=" + listeJoueurs +
                ", listeEvenement=" + listeEvenement +
                ", listeJoueurParEvenement=" + listeJoueurParEvenement +
                ", listeEvenementParJoueur=" + listeEvenementParJoueur +
                '}';
    }
}
