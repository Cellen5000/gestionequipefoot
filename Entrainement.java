public class Entrainement extends Evenement{
    TypeEvenement typeEntrainement = TypeEvenement.ENTRAINEMENT;
    SelectionPourEntrainement sE;
    String nomEquipe;

    public Entrainement(String lieu) {
        super(lieu);
    }

    public Entrainement(String lieu, SelectionPourEntrainement sE){
        super(lieu);
        this.sE = sE;
        nomEquipe = sE.getEquipeAEntrainer().getNomEquipe();

        for ( JoueurAutreQueGardien j : sE.listeJoueursEquipeSelectionnables){
            j.sEntrainer();
            j.nbreEntrainementsJoues ++;
        }
        sE.getEquipeAEntrainer().getGardien().sEntrainer();
        sE.getEquipeAEntrainer().getGardien().nbreEntrainementsJoues ++;
    }


    public TypeEvenement getTypeEntrainement() {
        return typeEntrainement;
    }

    public void setTypeEntrainement(TypeEvenement typeEntrainement) {
        this.typeEntrainement = typeEntrainement;
    }

    public SelectionPourEntrainement getsE() {
        return sE;
    }

    public void setsE(SelectionPourEntrainement sE) {
        this.sE = sE;
    }

    public String getNomEquipe() {
        return nomEquipe;
    }

    public void setNomEquipe(String nomEquipe) {
        this.nomEquipe = nomEquipe;
    }

    @Override
    public String toString() {
        return "Entrainement{" +
                "Numero evenement='" + numero + '\'' +
                ", typeEntrainement=" + typeEntrainement +
                ", lieu='" + lieu + '\'' +
                '}';
    }
}
