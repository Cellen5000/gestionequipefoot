public interface IBlesser {
    /**
     * Méthode déclarée et à redéfinir qui permet de retirer de la stamina à un joueur blessé
     */
    public void seBlesser();

    /**
     * Méthode déclarée et à redéfinir qui permet à joueur prêt à prendre de ne plus être blessé
     */
    public void guerir();

    /**
     * Méthode déclarée et à redéfinir qui permet d'attribuer une blessure à la liste de blessures du joueur
     */

    public void ajouterBlessureAListeBlessures(Blessure blessureCourante);
}
