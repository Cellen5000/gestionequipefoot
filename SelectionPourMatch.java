import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class SelectionPourMatch extends SelectionPourEntrainement implements IPositionAttribuable{
    int minSeuilStaminaPourMatch = 7;
    Equipe equipeMatch;
    Map<Position,JoueurAutreQueGardien> selection = new HashMap<>();
    HashSet<Position> listeDePositions = new HashSet<>();



    public SelectionPourMatch(Equipe e) {
        super(e);
        this.equipeMatch = e;
        attribuerJoueurA1Position(listeJoueursEquipeSelectionnables);
    }

    /**
     * Méthode déclarée et à redéfinir qui permet de savoir si un joueur d'une équipe est séléctionnable pour aller en Match
     *
     * @param j liste un joueur à évaluer pour une sélection
     * @return true s'il répond aux conditions suivantes:
     * Pour être selectionné: le joueur doit être disponible, ne pas être blessé, avoir un stamina sup. ou égal à 7
     */
    @Override
    public boolean isSelectionnable(JoueurAutreQueGardien j) {
        if ((j.isDisponible) && (!j.isBlesse) && (j.stamina >=minSeuilStaminaPourMatch)){
            return true;
        } else{
            j.nbreMatchsManques += 1;
            j.nePasEtrePresent();
            return false;
        }

    }

    /**
     * Méthode déclarée et à redéfinir qui permet d'attribuer un joueur pour chaque position pour aller en Match
     *
     * @param listeJoueursEquipeSelectionnables liste sans doublons de joueurs d'une équipe sélectionnée
     *                                         Pour être selectionné: le joueur doit avoir dans sa specialisation  (+ la stamina et la qlitéJeu les + hauts)
     */
    @Override
    public void attribuerJoueurA1Position(HashSet<JoueurAutreQueGardien> listeJoueursEquipeSelectionnables) {
        listerPositions();

        for ( Position p : listeDePositions) {
            for (JoueurAutreQueGardien j : listeJoueursEquipeSelectionnables) {
                if ((!j.isSelectionne()) && (j.qliteJeu >= 7)) {
                    for (Position position : j.specialisation) {
                        if (position == p) {
                            selection.putIfAbsent(p, j);
                            j.setSelectionne(true);
                            j.setpMatch(p);                        }
                    }
                }
            }
        }
    }

    public void listerPositions(){
        listeDePositions.add(Position.ATTAQUANT_CENTRE);
        listeDePositions.add(Position.ATTAQUANT_GAUCHE);
        listeDePositions.add(Position.ATTAQUANT_DROIT);
        listeDePositions.add(Position.MILIEU_CENTRE_GAUCHE);
        listeDePositions.add(Position.MILIEU_CENTRE_DROIT);
        listeDePositions.add(Position.MILIEU_LATTERAL_GAUCHE);
        listeDePositions.add(Position.MILIEU_LATTERAL_DROIT);
        listeDePositions.add(Position.DEFENSEUR_CENTRE);
        listeDePositions.add(Position.DEFENSEUR_GAUCHE);
        listeDePositions.add(Position.DEFENSEUR_DROIT);
    }

    public int getMinSeuilStaminaPourMatch() {
        return minSeuilStaminaPourMatch;
    }

    public void setMinSeuilStaminaPourMatch(int minSeuilStaminaPourMatch) {
        this.minSeuilStaminaPourMatch = minSeuilStaminaPourMatch;
    }

    public Equipe getEquipeMatch() {
        return equipeMatch;
    }

    public void setEquipeMatch(Equipe equipeMatch) {
        this.equipeMatch = equipeMatch;
    }

    public Map<Position, JoueurAutreQueGardien> getSelection() {
        return selection;
    }

    public void setSelection(Map<Position, JoueurAutreQueGardien> selection) {
        this.selection = selection;
    }

    public HashSet<Position> getListeDePositions() {
        return listeDePositions;
    }

    public void setListeDePositions(HashSet<Position> listeDePositions) {
        this.listeDePositions = listeDePositions;
    }

    @Override
    public String toString() {
        return "SelectionPourMatch{" +
                "minSeuilStaminaPourMatch=" + minSeuilStaminaPourMatch +
                ", equipeMatch=" + equipeMatch +
                ", selection=" + selection +
                '}';
    }
}
