import java.util.Date;
import java.util.Random;

public class Gardien extends Joueur{
    private Role role = Role.GARDIEN;
    int nbreBallesRetenuesTotal = 0;
    int nbreButEncaissesTotal = 0;

    int nbreButEncaissesEn1Match = 0;
    int nbreBallesRetenuesEn1Match = 0;

    public Gardien(){
        super();
    }


    /*
    public Gardien(String prenom, String nom, LocalDate dateDeNaissance, int nbreCartonsJaunesTotal, int nbreCartonsRougesTotal, int nbreMatchsManques, int nbreMatchJoues, int nbreEntrainementsJoues, int nbreEntrainementsManques, int nbreFautesCommisesTotales, int nbreFautesSubiesTotal, int nbreBlessuresTotal, int nbreBallesRetenuesTotal, int nbreButEncaissesTotal, int nbreBallesRetenuesEn1Match) {
        super(prenom, nom, dateDeNaissance, nbreCartonsJaunesTotal, nbreCartonsRougesTotal, nbreMatchsManques, nbreMatchJoues, nbreEntrainementsJoues, nbreEntrainementsManques, nbreFautesCommisesTotales, nbreFautesSubiesTotal, nbreBlessuresTotal);
        this.nbreBallesRetenuesTotal = nbreBallesRetenuesTotal;
        this.nbreButEncaissesTotal = nbreButEncaissesTotal;
        this.nbreBallesRetenuesEn1Match = nbreBallesRetenuesEn1Match;
    }
*/
    public Gardien(String prenom, String nom){
        super(prenom, nom);
    }

    public Gardien(String prenom, String nom, int nbreCartonsJaunesTotal, int nbreCartonsRougesTotal, int nbreMatchsManques, int nbreMatchJoues, int nbreEntrainementsJoues, int nbreEntrainementsManques, int nbreFautesSubiesTotal, int nbreFautesCommisesTotales, int nbreBallesRetenuesTotal, int nbreButEncaissesTotal) {
        super(prenom, nom, nbreCartonsJaunesTotal, nbreCartonsRougesTotal, nbreMatchsManques, nbreMatchJoues, nbreEntrainementsJoues, nbreEntrainementsManques, nbreFautesSubiesTotal, nbreFautesCommisesTotales);
        this.nbreBallesRetenuesTotal = nbreBallesRetenuesTotal;
        this.nbreButEncaissesTotal = nbreButEncaissesTotal;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public int getNbreBallesRetenuesTotal() {
        return nbreBallesRetenuesTotal;
    }

    public void setNbreBallesRetenuesTotal(int nbreBallesRetenuesTotal) {
        this.nbreBallesRetenuesTotal = nbreBallesRetenuesTotal;
    }

    public int getNbreButEncaissesTotal() {
        return nbreButEncaissesTotal;
    }

    public void setNbreButEncaissesTotal(int nbreButEncaissesTotal) {
        this.nbreButEncaissesTotal = nbreButEncaissesTotal;
    }

    public int getNbreBallesRetenuesEn1Match() {
        return nbreBallesRetenuesEn1Match;
    }

    public void setNbreBallesRetenuesEn1Match(int nbreBallesRetenuesEn1Match) {
        this.nbreBallesRetenuesEn1Match = nbreBallesRetenuesEn1Match;
    }

    public int getNbreButEncaissesEn1Match() {
        return nbreButEncaissesEn1Match;
    }

    public void setNbreButEncaissesEn1Match(int nbreButEncaissesEn1Match) {
        this.nbreButEncaissesEn1Match = nbreButEncaissesEn1Match;
    }

    @Override
    public String toString() {
        return "Gardien{" +
                "role=" + role +
                ", prenom='" + super.getPrenom() + '\'' +
                ", nom='" + super.getNom() + '\'' +
                //", dateDeNaissance=" + super.getDateDeNaissance() +
                ", nbreBallesRetenuesTotal=" + nbreBallesRetenuesTotal +
                ", nbreButEncaissesTotal=" + nbreButEncaissesTotal +
                ", nbreBallesRetenuesEn1Match=" + nbreBallesRetenuesEn1Match +
                ", nbreButEncaissesEn1Match="+ nbreButEncaissesEn1Match +
                ", nbreCartonsJaunesTotal=" + nbreCartonsJaunesTotal +
                ", nbreCartonsRougesTotal=" + nbreCartonsRougesTotal +
                ", nbreMatchsManques=" + nbreMatchsManques +
                ", nbreMatchJoues=" + nbreMatchJoues +
                ", nbreEntrainementsJoues=" + nbreEntrainementsJoues +
                ", nbreEntrainementsManques=" + nbreEntrainementsManques +
                ", nbreFautesSubiesTotal=" + nbreFautesSubiesTotal +
                ", listeBlessures=" + listeBlessures +
                ", nbreBlessuresTotal=" + nbreBlessuresTotal +
                ", isBlesse=" + isBlesse +
                ", IsDisponible=" + isDisponible +
                ", stamina=" + stamina +
                ", qliteJeu=" + qliteJeu +
                ", maxStamina=" + maxStamina +
                ", maxQliteJeu=" + maxQliteJeu +
                ", nbreCartonsJaunesEn1Match=" + getNbreCartonsJaunesEn1Match() +
                ", nbreCartonsRougesEn1Match=" + nbreCartonsRougesEn1Match +
                ", nbreFauteSubiesEn1Match=" + nbreFauteSubiesEn1Match +
                ", nbreFautesCommisesTotales=" + nbreFautesCommisesTotales +
                ", nbreFautesCommisesEn1Match=" + nbreFautesCommisesEn1Match +
                ", nbreMatchesTotal=" + nbreMatchesTotal +
                ", nbreEntrainementsTotal=" + nbreEntrainementsTotal +
                '}';
    }

    /**
     * Méthode déclarée et à redéfinir qui permet à un joueur de s'entrainer : augmenter sa qualité de Jeu et baisser sa stamina
     * et d'attribuer des valeurs aléatoires
     * ex: pour un gardien : le nombre de balles retenues, nombre de balles non rattrapées
     * pour un joueur : le nombre de balles marquées,  nombre de passes décisives
     */
    @Override
    public void jouer() {
        if ((stamina <= maxStamina) && (stamina > 0)) {
            stamina = stamina - 1;
        }
        if ((qliteJeu <= maxQliteJeu) && (qliteJeu > 0)) {
            qliteJeu = qliteJeu + 1;
        }
        Random rand1 = new Random();
        Random rand2 = new Random();
        Random rand3 = new Random();
        setNbreFauteSubiesEn1Match(rand3.nextInt(0,4));
        setNbreBallesRetenuesEn1Match(rand1.nextInt(1,9));
        setNbreButEncaissesEn1Match(rand2.nextInt(1,4));
        nbreBallesRetenuesTotal += nbreBallesRetenuesEn1Match;
        nbreButEncaissesTotal += nbreButEncaissesEn1Match;
        nbreFautesSubiesTotal += nbreFauteSubiesEn1Match;

    }

    /**
     * Méthode déclarée et à redéfinir qui permet quand un joueur d'initialiser ses compteurs de fautes, cartons avant de commencer à jouer un match
     */
    @Override
    public void initCompteursPersoPourMatch() {
        nbreCartonsJaunesEn1Match = 0;
        nbreCartonsRougesEn1Match = 0;
        nbreFautesCommisesEn1Match = 0;
        nbreFauteSubiesEn1Match = 0;
        nbreBallesRetenuesEn1Match =0;
        nbreButEncaissesEn1Match = 0;
    }

    /**
     * Méthode déclarée et à redéfinir qui permet d'afficher les statistiques du Joueur
     */
    @Override
    public void afficherStat() {
        calculPourStat();
        System.out.printf("STATISTIQUES DE %s %s : \n", getPrenom(), getNom() );
        System.out.println("Son role est celui du "+role);

        System.out.printf("Sa stamina est de %d sur %d. \n", stamina,maxStamina);
        System.out.printf("Sa qualité de jeu est de %d sur %d. \n", qliteJeu,getMaxQliteJeu());
        System.out.printf("Sur %d entrainements :", nbreEntrainementsTotal);
        System.out.printf("Il a participé à %d entrainements.\n", nbreEntrainementsJoues);
        System.out.printf("Il a manqué %d entrainements.\n", nbreEntrainementsManques);
        System.out.printf("Sur %d matches :", nbreMatchesTotal);
        System.out.printf("Il a participé à %d matches.\n", nbreMatchJoues);
        System.out.printf("Il a manqué %d matches.\n", nbreMatchsManques);

        System.out.printf("Depuis le début de sa carrière, sur %d ballons : \n", (nbreBallesRetenuesTotal + nbreButEncaissesTotal));
        System.out.printf("il en a retenu %d et il a encaissé %d buts. \n", nbreBallesRetenuesTotal, nbreCartonsRougesTotal);
        System.out.printf("Au dernier match, sur %d ballons : \n", nbreBallesRetenuesEn1Match + nbreButEncaissesEn1Match);
        System.out.printf("il en a retenu %d et il a encaissé %d buts. \n", nbreBallesRetenuesEn1Match, nbreButEncaissesEn1Match);

        System.out.printf("Depuis le début de sa carrière, il a commis %d fautes: \n", nbreFautesCommisesTotales);
        System.out.printf("dont %d cartons jaunes et %d cartons rouges. \n", nbreCartonsJaunesTotal, nbreCartonsRougesTotal);
        System.out.printf("Au dernier match, il en a commise %d : \n", nbreFautesCommisesEn1Match);
        System.out.printf("dont %d cartons jaunes et %d cartons rouges. \n", nbreCartonsJaunesEn1Match, nbreCartonsRougesEn1Match);
        System.out.printf("Depuis le début de sa carrière, il a subi %d fautes \n", nbreFautesSubiesTotal);
        System.out.printf("Au dernier match, il en a subi %d. \n", nbreFauteSubiesEn1Match);
        System.out.printf("Depuis le début de sa carrière il a eu %d blessures.\n",nbreBlessuresTotal);
        System.out.printf("Actuellement, est-il blessé ? %b", isBlesse);
    }
}
