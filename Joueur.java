import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Joueur extends Personne implements IBlesser, IJouer, IAfficherStatistiques{
    int nbreCartonsJaunesTotal=0;
    int nbreCartonsRougesTotal=0;
    int nbreMatchsManques=0;
    int nbreMatchJoues=0;
    int nbreEntrainementsJoues=0;
    int nbreEntrainementsManques=0;
    int nbreFautesSubiesTotal=0;

    Blessure blessureCourante;
    HashSet<Blessure> listeBlessures = new HashSet<>();
    int nbreBlessuresTotal;



    boolean isBlesse= false;
    boolean isDisponible = true;
    boolean isSelectionne = false;

    int stamina = 10;
    int qliteJeu = 7;
    int maxStamina = 10;
    int maxQliteJeu = 10;
    int nbreCartonsJaunesEn1Match = 0;
    int nbreCartonsRougesEn1Match = 0;
    int nbreFauteSubiesEn1Match = 0;
    int nbreFautesCommisesTotales;
    int nbreFautesCommisesEn1Match;
    int nbreMatchesTotal;
    int nbreEntrainementsTotal;

    Set<Faute> listeFautesParMatch = new HashSet<>();



    public Joueur(){
        super();
    }

    public Joueur(String prenom, String nom) {
        super(prenom, nom);
    }


    public Joueur(String prenom, String nom, int nbreCartonsJaunesTotal, int nbreCartonsRougesTotal, int nbreMatchsManques, int nbreMatchJoues, int nbreEntrainementsJoues, int nbreEntrainementsManques, int nbreFautesSubiesTotal, int nbreFautesCommisesTotales) {
        super(prenom, nom);
        this.nbreCartonsJaunesTotal = nbreCartonsJaunesTotal;
        this.nbreCartonsRougesTotal = nbreCartonsRougesTotal;
        this.nbreMatchsManques = nbreMatchsManques;
        this.nbreMatchJoues = nbreMatchJoues;
        this.nbreEntrainementsJoues = nbreEntrainementsJoues;
        this.nbreEntrainementsManques = nbreEntrainementsManques;
        this.nbreFautesSubiesTotal = nbreFautesSubiesTotal;
        this.nbreFautesCommisesTotales = nbreFautesCommisesTotales;
        calculPourStat();
    }
/*
    public Joueur(String prenom, String nom, Date dateDeNaissance, int nbreCartonsJaunesTotal, int nbreCartonsRougesTotal, int nbreMatchsManques, int nbreMatchJoues, int nbreEntrainementsJoues, int nbreEntrainementsManques, int nbreFautesCommisesTotales, int nbreFautesSubiesTotal, int nbreBlessuresTotal) {
        super(prenom, nom, dateDeNaissance);
        this.nbreCartonsJaunesTotal = nbreCartonsJaunesTotal;
        this.nbreCartonsRougesTotal = nbreCartonsRougesTotal;
        this.nbreMatchsManques = nbreMatchsManques;
        this.nbreMatchJoues = nbreMatchJoues;
        this.nbreEntrainementsJoues = nbreEntrainementsJoues;
        this.nbreEntrainementsManques = nbreEntrainementsManques;
        this.nbreFautesSubiesTotal = nbreFautesSubiesTotal;
        this.nbreBlessuresTotal = nbreBlessuresTotal;
    }
*/
    public int getNbreCartonsJaunesTotal() {
        return nbreCartonsJaunesTotal;
    }

    public void setNbreCartonsJaunesTotal(int nbreCartonsJaunesTotal) {
        this.nbreCartonsJaunesTotal = nbreCartonsJaunesTotal;
    }

    public int getNbreCartonsRougesTotal() {
        return nbreCartonsRougesTotal;
    }

    public void setNbreCartonsRougesTotal(int nbreCartonsRougesTotal) {
        this.nbreCartonsRougesTotal = nbreCartonsRougesTotal;
    }

    public int getNbreMatchsManques() {
        return nbreMatchsManques;
    }

    public void setNbreMatchsManques(int nbreMatchsManques) {
        this.nbreMatchsManques = nbreMatchsManques;
    }

    public int getNbreMatchJoues() {
        return nbreMatchJoues;
    }

    public void setNbreMatchJoues(int nbreMatchJoues) {
        this.nbreMatchJoues = nbreMatchJoues;
    }

    public int getNbreEntrainementsJoues() {
        return nbreEntrainementsJoues;
    }

    public void setNbreEntrainementsJoues(int nbreEntrainementsJoues) {
        this.nbreEntrainementsJoues = nbreEntrainementsJoues;
    }

    public int getNbreEntrainementsManques() {
        return nbreEntrainementsManques;
    }

    public void setNbreEntrainementsManques(int nbreEntrainementsManques) {
        this.nbreEntrainementsManques = nbreEntrainementsManques;
    }

    public int getNbreFautesSubiesTotal() {
        return nbreFautesSubiesTotal;
    }

    public void setNbreFautesSubiesTotal(int nbreFautesSubiesTotal) {
        this.nbreFautesSubiesTotal = nbreFautesSubiesTotal;
    }

    public int getNbreFauteSubiesEn1Match() {
        return nbreFauteSubiesEn1Match;
    }

    public void setNbreFauteSubiesEn1Match(int nbreFauteSubiesEn1Match) {
        this.nbreFauteSubiesEn1Match = nbreFauteSubiesEn1Match;
    }

    public int getNbreBlessuresTotal() {
        return nbreBlessuresTotal;
    }

    public void setNbreBlessuresTotal(int nbreBlessuresTotal) {
        this.nbreBlessuresTotal = nbreBlessuresTotal;
    }

    public boolean isBlesse() {
        return isBlesse;
    }

    public void setBlesse(boolean blesse) {
        isBlesse = blesse;
    }

    public boolean isDisponible() {
        return isDisponible;
    }

    public void setDisponible(boolean disponible) {
        isDisponible = disponible;
    }

    public int getStamina() {
        return stamina;
    }

    public void setStamina(int stamina) {
        this.stamina = stamina;
    }

    public int getQliteJeu() {
        return qliteJeu;
    }

    public void setQliteJeu(int qliteJeu) {
        this.qliteJeu = qliteJeu;
    }

    public int getMaxStamina() {
        return maxStamina;
    }

    public void setMaxStamina(int maxStamina) {
        this.maxStamina = maxStamina;
    }

    public int getMaxQliteJeu() {
        return maxQliteJeu;
    }

    public void setMaxQliteJeu(int maxQliteJeu) {
        this.maxQliteJeu = maxQliteJeu;
    }

    public int getNbreCartonsJaunesEn1Match() {
        return nbreCartonsJaunesEn1Match;
    }

    public void setNbreCartonsJaunesEn1Match(int nbreCartonsJaunesEn1Match) {
        this.nbreCartonsJaunesEn1Match = nbreCartonsJaunesEn1Match;
    }

    public int getNbreCartonsRougesEn1Match() {
        return nbreCartonsRougesEn1Match;
    }

    public void setNbreCartonsRougesEn1Match(int nbreCartonsRougesEn1Match) {
        this.nbreCartonsRougesEn1Match = nbreCartonsRougesEn1Match;
    }

    public int getNbreFautesCommisesTotales() {
        return nbreFautesCommisesTotales;
    }

    public int getNbreFautesCommisesEn1Match() {
        return nbreFautesCommisesEn1Match;
    }


    public int getNbreMatchesTotal() {
        return nbreMatchesTotal;
    }

    public int getNbreEntrainementsTotal() {
        return nbreEntrainementsTotal;
    }

    public HashSet<Blessure> getListeBlessures() {
        return listeBlessures;
    }

    public void setListeBlessures(HashSet<Blessure> listeBlessures) throws ExceptionParticuliere{
        if (listeFautesParMatch != null){
            this.listeBlessures = listeBlessures;
        } else{
            throw new ExceptionParticuliere("La liste de fautes par match est vide, on ne peut pas l'associer à celle du client");
        }
    }

    public boolean isSelectionne() {
        return isSelectionne;
    }

    public void setSelectionne(boolean selectionne) {
        isSelectionne = selectionne;
    }

    public void setNbreFautesCommisesTotales(int nbreFautesCommisesTotales) {
        this.nbreFautesCommisesTotales = nbreFautesCommisesTotales;
    }

    public void setNbreFautesCommisesEn1Match(int nbreFautesCommisesEn1Match) {
        this.nbreFautesCommisesEn1Match = nbreFautesCommisesEn1Match;
    }

    public void setNbreMatchesTotal(int nbreMatchesTotal) {
        this.nbreMatchesTotal = nbreMatchesTotal;
    }

    public void setNbreEntrainementsTotal(int nbreEntrainementsTotal) {
        this.nbreEntrainementsTotal = nbreEntrainementsTotal;
    }


    public Set<Faute> getListeFautesParMatch() {
        return listeFautesParMatch;
    }

    public void setListeFautesParMatch(Set<Faute> listeFautesParMatch){
            this.listeFautesParMatch = listeFautesParMatch;
    }

    public Blessure getBlessureCourante() {
        return blessureCourante;
    }

    public void setBlessureCourante(Blessure blessureCourante) {
        this.blessureCourante = blessureCourante;
    }
    /**
     * Méthode redéfinie qui permet d'attribuer une blessure à la liste de blessures du joueur
     */
    public void ajouterBlessureAListeBlessures(Blessure blessureCourante){
        this.blessureCourante = blessureCourante;
        this.listeBlessures.add(blessureCourante);
    }

    @Override
    public String toString() {
        return "Joueur{" +
                "prenom='" + super.getPrenom() + '\'' +
                ", nom='" + super.getNom() + '\'' +
               // ", dateDeNaissance=" + super.getDateDeNaissance() +
                "nbreCartonsJaunesTotal=" + nbreCartonsJaunesTotal +
                ", nbreCartonsRougesTotal=" + nbreCartonsRougesTotal +
                ", nbreMatchsManques=" + nbreMatchsManques +
                ", nbreMatchJoues=" + nbreMatchJoues +
                ", nbreEntrainementsJoues=" + nbreEntrainementsJoues +
                ", nbreEntrainementsManques=" + nbreEntrainementsManques +
                ", nbreFautesSubiesTotal=" + nbreFautesSubiesTotal +
                ", nbreFauteSubiesEn1Match=" + nbreFauteSubiesEn1Match +
                ", nbreBlessuresTotal=" + nbreBlessuresTotal +
                ", isBlesse=" + isBlesse +
                ", IsDisponible=" + isDisponible +
                ", stamina=" + stamina +
                ", qliteJeu=" + qliteJeu +
                ", maxStamina=" + maxStamina +
                ", maxQliteJeu=" + maxQliteJeu +
                ", nbreCartonsJaunesEn1Match=" + nbreCartonsJaunesEn1Match +
                ", nbreCartonsRougesEn1Match=" + nbreCartonsRougesEn1Match +
                ", nbreFautesCommisesTotales=" + nbreFautesCommisesTotales +
                ", nbreFautesCommisesEn1Match=" + nbreFautesCommisesEn1Match +
                ", nbreMatchesTotal=" + nbreMatchesTotal +
                ", nbreEntrainementsTotal=" + nbreEntrainementsTotal +
                '}';
    }

    /**
     * Méthode déclarée et à redéfinir qui permet de retirer de la stamina à un joueur blessé
     */
    @Override
    public void seBlesser() {
        if ((stamina <= maxStamina) && (stamina > 0)) {
            setBlesse(true);
            stamina = 3;
            nbreBlessuresTotal ++;
        }
        if ((qliteJeu <= maxQliteJeu) && (qliteJeu > 0)) {
            qliteJeu = qliteJeu - 1;
        }
    }


    /**
     * Méthode déclarée et à redéfinir qui permet à joueur prêt à prendre de ne plus être blessé
     */
    @Override
    public void guerir() {
        stamina = maxStamina;
        setBlesse(false);

    }

    /**
     * Méthode déclarée et à redéfinir qui permet à un joueur de s'entrainer : augmenter sa qualité de Jeu et baisser sa stamina
     */
    @Override
    public void sEntrainer() {
        if ((stamina <= maxStamina) && (stamina > 0)) {
            stamina = stamina - 1;
        }
        if ((qliteJeu <= maxQliteJeu) && (qliteJeu > 0)) {
            qliteJeu = qliteJeu + 1;
        }
    }

    /**
     * Méthode déclarée et à redéfinir qui permet à un joueur de s'entrainer : augmenter sa qualité de Jeu et baisser sa stamina
     * et d'attribuer des valeurs aléatoires
     * ex: pour un gardien : le nombre de balles retenues, nombre de balles non rattrapées
     * pour un joueur : le nombre de balles marquées,  nombre de passes décisives
     */
    @Override
    public void jouer() {
        if ((stamina <= maxStamina) && (stamina > 0)) {
            stamina = stamina - 1;
        }
        if ((qliteJeu <= maxQliteJeu) && (qliteJeu > 0)) {
            qliteJeu = qliteJeu + 1;
        }
    }

    /**
     * Méthode déclarée et à redéfinir qui permet quand un joueur de ne pas présent , il y a une baisse sa qualité de jeu et augmente sa stamina
     */
    @Override
    public void nePasEtrePresent() {
        if ((stamina <= maxStamina) && (stamina > 0)) {
            stamina = stamina + 1;
        }
        if ((qliteJeu <= maxQliteJeu) && (qliteJeu > 0)) {
            qliteJeu = qliteJeu - 1;
        }

    }

    /**
     * Méthode déclarée et à redéfinir qui permet quand un joueur d'initialiser ses compteurs de fautes, cartons avant de commencer à jouer un match
     */
    @Override
    public void initCompteursPersoPourMatch() {
        nbreCartonsJaunesEn1Match = 0;
        nbreCartonsRougesEn1Match = 0;
        nbreFautesCommisesEn1Match = 0;
        nbreFauteSubiesEn1Match = 0;
    }


    /**
     * Méthode déclarée et à redéfinir qui permet d'afficher les statistiques du Joueur
     */
    @Override
    public void afficherStat() {
        calculPourStat();
        System.out.printf("STATISTIQUES DE %s %s : \n", getPrenom(), getNom() );
        //System.out.printf("Son role est %", );
        // speciatlisation
        // Sa position occupée au dernier match:
        // Gardien : Balles retenues + BallesManquees
        // Joueur : Passes Decisives
        // Joueur : TirsTotaux : TirsReussis + TirsManques
        System.out.printf("Sa stamina est de %d sur %d. \n", stamina,maxStamina);
        System.out.printf("Sa qualité de jeu est de %d sur %d. \n", qliteJeu,getMaxQliteJeu());
        System.out.printf("Sur %d entrainements :", nbreEntrainementsTotal);
        System.out.printf("Il a participé à %d entrainements.\n", nbreEntrainementsJoues);
        System.out.printf("Il a manqué %d entrainements.\n", nbreEntrainementsManques);
        System.out.printf("Sur %d matches :", nbreMatchesTotal);
        System.out.printf("Il a participé à %d matches.\n", nbreMatchJoues);
        System.out.printf("Il a manqué %d matches.\n", nbreMatchsManques);
        System.out.printf("Depuis le début de sa carrière, il a commis %d fautes: \n", nbreFautesCommisesTotales);
        System.out.printf("dont %d cartons jaunes et %d cartons rouges. \n", nbreCartonsJaunesTotal, nbreCartonsRougesTotal);
        System.out.printf("Au dernier match, il en a commise %d : \n", nbreFautesCommisesEn1Match);
        System.out.printf("dont %d cartons jaunes et %d cartons rouges. \n", nbreCartonsJaunesEn1Match, nbreCartonsRougesEn1Match);
        System.out.printf("Depuis le début de sa carrière, il a subi %d fautes \n", nbreFautesSubiesTotal);
        System.out.printf("Au dernier match, il en a subi %d. \n", nbreFauteSubiesEn1Match);
        System.out.printf("Depuis le début de sa carrière il a eu %d blessures.\n",nbreBlessuresTotal);
        System.out.printf("Actuellement, est-il blessé ? %b", isBlesse);


    }

    /**
     * Méthode déclarée et à redéfinir qui permet de faire des calculs pour les statistiques
     */
    @Override
    public void calculPourStat() {
        nbreFautesCommisesTotales = nbreCartonsJaunesTotal + nbreCartonsRougesTotal;
        nbreFautesCommisesEn1Match= nbreCartonsJaunesEn1Match + nbreCartonsRougesEn1Match;
        nbreMatchesTotal = nbreMatchJoues + nbreMatchsManques;
        nbreEntrainementsTotal= nbreEntrainementsJoues + nbreEntrainementsManques;
    }
}
