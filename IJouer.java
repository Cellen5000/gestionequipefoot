public interface IJouer {
    /**
     * Méthode déclarée et à redéfinir qui permet à un joueur de s'entrainer : augmenter sa qualité de Jeu et baisser sa stamina
     */
    public void sEntrainer();
    /**
     * Méthode déclarée et à redéfinir qui permet à un joueur de s'entrainer : augmenter sa qualité de Jeu et baisser sa stamina
     * et d'attribuer des valeurs aléatoires
     * ex: pour un gardien : le nombre de balles retenues, nombre de balles non rattrapées
     *     pour un joueur : le nombre de balles marquées,  nombre de passes décisives
     */
    public void jouer();

    /**
     * Méthode déclarée et à redéfinir qui permet quand un joueur de ne pas présent , il y a une baisse sa qualité de jeu et augmente sa stamina
     */
    public void nePasEtrePresent();

    /**
     * Méthode déclarée et à redéfinir qui permet quand un joueur d'initialiser ses compteurs de fautes, cartons avant de commencer à jouer un match
     */
    public void initCompteursPersoPourMatch();
}

