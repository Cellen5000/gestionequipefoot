import java.util.Arrays;
import java.util.Date;
import java.util.Random;

public class JoueurAutreQueGardien extends Joueur {

    private Role role = Role.AUTRE_JOUEUR;
    Position[] specialisation = new Position[3];
    Position p1;
    Position p2;
    Position p3;
    Position pMatch;
    PiedFort piedFort;

    int nbreTentativesTirsManquesEn1Match = 0;
    int nbreBallesMarqueesEn1Match = 0;
    int nbrePassesDecisivesEn1Match = 0;

    int nbrePassesDecisivesTotales = 0;

    int nbreBallesMarqueesTotales = 0;

    int nbreTentativesTirsManquesTotales = 0;


    public JoueurAutreQueGardien() {
        super();
    }


    public JoueurAutreQueGardien(String prenom, String nom, Position p1, Position p2, Position p3, PiedFort piedFort) {
        super(prenom, nom);
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.piedFort = piedFort;
        specialisation[0] = p1;
        specialisation[1] = p2;
        specialisation[2] = p3;

    }
/*
    public JoueurAutreQueGardien(String prenom, String nom, int nbreCartonsJaunesTotal, int nbreCartonsRougesTotal, int nbreMatchsManques, int nbreMatchJoues, int nbreEntrainementsJoues, int nbreEntrainementsManques, int nbreFautesSubiesTotal, int nbreFautesCommisesTotales, Position p1, Position p2, Position p3) {
        super(prenom, nom, nbreCartonsJaunesTotal, nbreCartonsRougesTotal, nbreMatchsManques, nbreMatchJoues, nbreEntrainementsJoues, nbreEntrainementsManques, nbreFautesSubiesTotal, nbreFautesCommisesTotales);
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        specialisation[0] = p1;
        specialisation[2] = p2;
        specialisation[3] = p3;
    }
*/

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Position[] getSpecialisation() {
        return specialisation;
    }

    public void setSpecialisation(Position[] specialisation) {
        this.specialisation = specialisation;
    }

    public Position getP1() {
        return p1;
    }

    public void setP1(Position p1) {
        this.p1 = p1;
        specialisation[0] = p1;
    }

    public Position getP2() {
        return p2;

    }

    public void setP2(Position p2) {
        this.p2 = p2;
        specialisation[1] = p2;
    }

    public Position getP3() {
        return p3;
    }

    public void setP3(Position p3) {
        this.p3 = p3;
        specialisation[2] = p3;
    }

    public PiedFort getPiedFort() {
        return piedFort;
    }

    public void setPiedFort(PiedFort piedFort) {
        this.piedFort = piedFort;
    }

    public Position getpMatch() {
        return pMatch;
    }

    public void setpMatch(Position pMatch) {
        this.pMatch = pMatch;
    }

    @Override
    public String toString() {
        return "JoueurAutreQueGardien{" +
                "role=" + role +
                ", prenom='" + super.getPrenom() + '\'' +
                ", nom='" + super.getNom() + '\'' +
                //", dateDeNaissance=" + super.getDateDeNaissance() +
                ", isBlesse=" + isBlesse +
                ", IsDisponible=" + isDisponible +
                ", stamina=" + stamina +
                ", qliteJeu=" + qliteJeu +
                ", maxStamina=" + maxStamina +
                ", maxQliteJeu=" + maxQliteJeu +
                ", nbreCartonsJaunes=" + getNbreCartonsJaunesEn1Match() +
                ", nbreCartonsRouges=" + nbreCartonsRougesEn1Match +
                //", nbreFautesTotales=" + nbreFautesTotales +
                //", nbreFautesEn1Match=" + nbreFautesEn1Match +
                ", nbreFautesSubiesTotal= "+ nbreFautesSubiesTotal +
                ", specialisation=" + Arrays.toString(specialisation) +
                ", position1=" + p1 +
                ", position2=" + p2 +
                ", position3=" + p3 +
                ", positionMatch=" + pMatch +
                ", pied fort=" + piedFort +

                '}';
    }

    /**
     * Méthode déclarée et à redéfinir qui permet à un joueur de s'entrainer : augmenter sa qualité de Jeu et baisser sa stamina
     * et d'attribuer des valeurs aléatoires
     * ex: pour un gardien : le nombre de balles retenues, nombre de balles non rattrapées
     * pour un joueur : le nombre de balles marquées,  nombre de passes décisives
     */
    @Override
    public void jouer() {
        if ((stamina <= maxStamina) && (stamina > 0)) {
            stamina = stamina - 1;
        }
        if ((qliteJeu <= maxQliteJeu) && (qliteJeu > 0)) {
            qliteJeu = qliteJeu + 1;
        }
        Random rand1 = new Random();
        Random rand2 = new Random();
        Random rand3 = new Random();
        setNbreFauteSubiesEn1Match(rand1.nextInt(0,4));
        nbreFautesSubiesTotal += nbreFauteSubiesEn1Match;
        setNbrePassesDecisivesEn1Match(rand2.nextInt(1, 4));
        nbrePassesDecisivesTotales += nbrePassesDecisivesEn1Match;
        setNbreTentativesTirsManquesEn1Match(rand3.nextInt(1, 4));
        nbreTentativesTirsManquesTotales += nbreTentativesTirsManquesEn1Match;
        if ((pMatch == Position.ATTAQUANT_CENTRE) || (pMatch == Position.ATTAQUANT_GAUCHE) || (pMatch == Position.ATTAQUANT_DROIT)){
            setNbreBallesMarqueesEn1Match(rand1.nextInt(0, 4));
            nbreBallesMarqueesTotales += nbreBallesMarqueesEn1Match;
        }
    }

    /**
     * Méthode déclarée et à redéfinir qui permet quand un joueur d'initialiser ses compteurs de fautes, cartons avant de commencer à jouer un match
     */
    @Override
    public void initCompteursPersoPourMatch() {
        nbreCartonsJaunesEn1Match = 0;
        nbreCartonsRougesEn1Match = 0;
        nbreFautesCommisesEn1Match = 0;
        nbreFauteSubiesEn1Match = 0;
        nbreBallesMarqueesEn1Match = 0;
        nbreTentativesTirsManquesEn1Match = 0;
        nbrePassesDecisivesEn1Match = 0;

    }

    /**
     * Méthode déclarée et à redéfinir qui permet d'afficher les statistiques du Joueur
     */
    @Override
    public void afficherStat() {
        calculPourStat();
        System.out.printf("STATISTIQUES DE %s %s : \n", getPrenom(), getNom() );
        System.out.println("Son role est celui d'un joueur (autre que gardien)");
        System.out.println("Ses spécialisations sont les suivantes :");
        for (Position p : specialisation){
            System.out.println(p);
        }
        System.out.println("La position attribuée lors du dernier match est "+ pMatch);
        // Gardien : Balles retenues + BallesManquees
        // Joueur : Passes Decisives
        // Joueur : TirsTotaux : TirsReussis + TirsManques
        System.out.printf("Sa stamina est de %d sur %d. \n", stamina,maxStamina);
        System.out.printf("Sa qualité de jeu est de %d sur %d. \n", qliteJeu,getMaxQliteJeu());
        System.out.printf("Sur %d entrainements :", nbreEntrainementsTotal);
        System.out.printf("Il a participé à %d entrainements.\n", nbreEntrainementsJoues);
        System.out.printf("Il a manqué %d entrainements.\n", nbreEntrainementsManques);
        System.out.printf("Sur %d matches :", nbreMatchesTotal);
        System.out.printf("Il a participé à %d matches.\n", nbreMatchJoues);
        System.out.printf("Il a manqué %d matches.\n", nbreMatchsManques);

        System.out.printf("Depuis le début de sa carrière:\n Sur %d tirs décisivifs: \n il a marqués %d buts et il a manqué %d buts.\n",nbreTentativesTirsManquesTotales + nbreBallesMarqueesTotales , nbreBallesMarqueesTotales,nbreTentativesTirsManquesTotales);
        System.out.printf("Au dernier match:\n Sur %d tirs décisivifs: \n il a marqués %d buts et il a manqué %d buts.\n",nbreTentativesTirsManquesEn1Match + nbreBallesMarqueesEn1Match , nbreBallesMarqueesEn1Match,nbreTentativesTirsManquesEn1Match);

        System.out.printf("Depuis le début de sa carrière:\n Sur %d passes décisives: \n",nbrePassesDecisivesTotales);
        System.out.printf("Au dernier match:\n Sur %d passes décisives: \n",nbrePassesDecisivesEn1Match);

        System.out.printf("Depuis le début de sa carrière, il a commis %d fautes: \n", nbreFautesCommisesTotales);
        System.out.printf("dont %d cartons jaunes et %d cartons rouges. \n", nbreCartonsJaunesTotal, nbreCartonsRougesTotal);
        System.out.printf("Au dernier match, il en a commise %d : \n", nbreFautesCommisesEn1Match);
        System.out.printf("dont %d cartons jaunes et %d cartons rouges. \n", nbreCartonsJaunesEn1Match, nbreCartonsRougesEn1Match);
        System.out.printf("Depuis le début de sa carrière, il a subi %d fautes \n", nbreFautesSubiesTotal);
        System.out.printf("Au dernier match, il en a subi %d. \n", nbreFauteSubiesEn1Match);
        System.out.printf("Depuis le début de sa carrière il a eu %d blessures.\n",nbreBlessuresTotal);
        System.out.printf("Actuellement, est-il blessé ? %b", isBlesse);


    }

    public int getNbreTentativesTirsManquesEn1Match() {
        return nbreTentativesTirsManquesEn1Match;
    }

    public void setNbreTentativesTirsManquesEn1Match(int nbreTentativesTirsManquesEn1Match) {
        this.nbreTentativesTirsManquesEn1Match = nbreTentativesTirsManquesEn1Match;
    }

    public int getNbreBallesMarqueesEn1Match() {
        return nbreBallesMarqueesEn1Match;
    }

    public void setNbreBallesMarqueesEn1Match(int nbreBallesMarqueesEn1Match) {
        this.nbreBallesMarqueesEn1Match = nbreBallesMarqueesEn1Match;
    }

    public int getNbrePassesDecisivesEn1Match() {
        return nbrePassesDecisivesEn1Match;
    }

    public void setNbrePassesDecisivesEn1Match(int nrePassesDecisivesEn1Match) {
        this.nbrePassesDecisivesEn1Match = nrePassesDecisivesEn1Match;
    }

    public int getNbrePassesDecisivesTotales() {
        return nbrePassesDecisivesTotales;
    }

    public void setNbrePassesDecisivesTotales(int nbrePassesDecisivesTotales) {
        this.nbrePassesDecisivesTotales = nbrePassesDecisivesTotales;
    }

    public int getNbreBallesMarqueesTotales() {
        return nbreBallesMarqueesTotales;
    }

    public void setNbreBallesMarqueesTotales(int nbreBallesMarqueesTotales) {
        this.nbreBallesMarqueesTotales = nbreBallesMarqueesTotales;
    }

    public int getNbreTentativesTirsManquesTotales() {
        return nbreTentativesTirsManquesTotales;
    }

    public void setNbreTentativesTirsManquesTotales(int nbreTentativesTirsManquesTotales) {
        this.nbreTentativesTirsManquesTotales = nbreTentativesTirsManquesTotales;
    }
}
