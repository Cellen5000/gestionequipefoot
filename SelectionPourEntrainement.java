import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class SelectionPourEntrainement implements ISelectionnable {
    Equipe equipeAEntrainer;
    HashSet<JoueurAutreQueGardien> listeJoueursEquipeComplete;
    HashSet<JoueurAutreQueGardien> listeJoueursEquipeSelectionnables = new HashSet<>();
    int minSeuilStamina = 8;

    public SelectionPourEntrainement() {
    }

    public SelectionPourEntrainement(Equipe equipe) {
        this.equipeAEntrainer = equipe;
        this.listeJoueursEquipeComplete = equipe.listeJoueurs;
        selectionner(listeJoueursEquipeComplete);

    }

    public Equipe getEquipeAEntrainer() {
        return equipeAEntrainer;
    }

    public void setEquipeAEntrainer(Equipe equipeAEntrainer) {
        this.equipeAEntrainer = equipeAEntrainer;
    }

    public HashSet<JoueurAutreQueGardien> getListeJoueursEquipeComplete() {
        return listeJoueursEquipeComplete;
    }

    public void setListeJoueursEquipeComplete(HashSet<JoueurAutreQueGardien> listeJoueursEquipeComplete) {
        this.listeJoueursEquipeComplete = listeJoueursEquipeComplete;
    }

    public HashSet<JoueurAutreQueGardien> getListeJoueursEquipeSelectionnables() {
        return listeJoueursEquipeSelectionnables;
    }

    public void setListeJoueursEquipeSelectionnables(HashSet<JoueurAutreQueGardien> listeJoueursEquipeSelectionnables) {
        this.listeJoueursEquipeSelectionnables = listeJoueursEquipeSelectionnables;
    }

    public int getMinSeuilStamina() {
        return minSeuilStamina;
    }

    public void setMinSeuilStamina(int minSeuilStamina) {
        this.minSeuilStamina = minSeuilStamina;
    }

    /**
     * Méthode déclarée et à redéfinir qui permet de savoir si un joueur d'une équipe est séléctionnable pour aller en Match
     *
     * @param j liste un joueur à évaluer pour une sélection
     * @return true s'il répond aux conditions suivantes:
     * Pour être selectionné: le joueur doit être disponible, ne pas être blessé, avoir un stamina sup. ou égal à 7
     */
    @Override
    public boolean isSelectionnable(JoueurAutreQueGardien j) {
        if ((j.isDisponible) && (!j.isBlesse) && (j.stamina >=minSeuilStamina)){
            return true;
        } else{
            j.nbreEntrainementsManques += 1;
            j.nePasEtrePresent();
            return false;
        }
    }

    /**
     * Méthode déclarée et à redéfinir qui permet de sélectionner joueurs d'une équipe de foot pour aller en Match
     *
     * @param listeJoueursEquipeComplete liste sans doublons de joueurs d'une équipe
     * @return true s'il répond aux conditions suivantes:
     * Pour être selectionné: le joueur doit être disponible, ne pas être blessé, avoir un stamina sup. ou égal à 7
     */
    @Override
    public void selectionner(HashSet<JoueurAutreQueGardien> listeJoueursEquipeComplete) {
        if (listeJoueursEquipeComplete != null){
            for (JoueurAutreQueGardien j : listeJoueursEquipeComplete){
                if (isSelectionnable(j)){
                   listeJoueursEquipeSelectionnables.add(j);
                }
            }
        }

    }

    @Override
    public String toString() {
        return "SelectionPourEntrainement{" +
                "equipeAEntrainer=" + equipeAEntrainer +
                ", listeJoueursEquipeComplete=" + listeJoueursEquipeComplete +
                ", listeJoueursEquipeSelectionnables=" + listeJoueursEquipeSelectionnables +
                ", minSeuilStamina=" + minSeuilStamina +
                '}';
    }
}
